// BASED ON BR | CONTRACT PRODUCT W/O MIRRORING USE CASE :

// RUN EVERY 12 PM
// Subscription to this product will be automatically terminated
// once it reaches expiry date

// 1) IN 'ACTIVATION' FLOW, These are EAI needs to be done :
//    EAI GET NOTIFIED FROM AOM AND STORE IT TO DB WITH FIELDS :
//    # MSISDN, ACTION, SCH TYPE, NEWMSISDN, INSTANCE_ID, CATALOGUE ID, ATTR VALUE, 
//    # PERIOD, START, END DATE, LIST OF MESSAGE

// 2) IN 'SEND REMINDER (END-DATE X)' FLOW, These are EAI needs to be done :
//    EAI SEND REMINDER VIA SMS FW TO SMSGW TO SUBSCRIBER

// 3) IN 'AOM ACTIVITY' FLOW, These are EAI needs to be done :
//    EAI GET NOTIFIED ABOUT CEASE, CHANGE OWNERSHIP, REMOVE OFFER, CHANGE MSISDN
//    EAI STORE THE NOTIFICATION IN DB

var moment = require ('moment');
var fs = require ('fs');

// FETCHING DATA SUBSCRIBER DAN DATA PELANGGAN
var DataSubscriberPaket = require ('./DataSubscriberPaketContractProduct.json');
var DataPelanggan = require ('./DataPelanggan.json');

// FILTER DATA SUBSCRIBER YANG EXPIRED MULAI DARI 00:00 -> 23:59 KARENA SCRIPT DIJALANKAN TIAP PUKUL 00:00
var DataYangAkanDiolah = DataSubscriberPaket.filter (item => {
  var expired_date = moment (item.last_activation_timestamp).add (
    item.durasi,
    item.satuan_durasi
  );
  var TengahPagiAtas = moment ().format ('YYYY-MM-DDT00:00:00+0000');
  var TengahMalamBawah = moment ().format ('YYYY-MM-DDT23:59:59+0000');
  return expired_date.isBetween (TengahPagiAtas, TengahMalamBawah, null, '(]');
});

// TERMINATE ROW DARI DATA SUBSCRIBER YANG EXPIRED CATAT DI LOG
DataYangAkanDiolah.map (item => {
  var namaPelanggan = DataPelanggan.find (itemPelanggan => {
    return itemPelanggan.msisdn === item.msisdn;
  }).nama;
  fs.appendFile (
    'LogContractProduct.txt',
    namaPelanggan +
      ' | ' +
      item.msisdn +
      ' dengan ' +
      item.nama_paket +
      ' berhasil diterminate\n',
    function (err) {
      if (err) throw err;
      console.log (
        namaPelanggan +
          ' | ' +
          item.msisdn +
          ' dengan ' +
          item.nama_paket +
          ' berhasil diterminate'
      );
    }
  );
});
