// BASED ON BR | CP W/ MIRROING USE CASE :

// RUN EVERY TIME (tbd)
// This is applicable for contract product whereby after end date, 
// subscriber will have the option to continue with the mirroring product.
// once it reaches expiry date

// 1) IN 'ACTIVATION' FLOW, 'These are EAI needs to be done' :
//    EAI GET NOTIFIED FROM AOM AND STORE IT TO DB

// 2) IN 'SEND CONFIRMATION' FLOW, 'These are EAI needs to be done' :
//    EAI CHECK IF 'ALMOST EXPIRED THING' NEED TO BE CONFIRMED AND SEND 
//    CONFIRMATION VIA SMS 

// 3) IN 'RECEIVE RESPONSE' FLOW, 'These are EAI needs to be done' :
//    EAI GET FORWARDED FROM MSGW FROM CUST RESPONSE
//    EAI DO KEYWORD MAPPING IN DB AND NOTIFY ORDERING TO AOM

// // AOM SEND NOTIF DIRECTLY TO SMSGW TO CUST ABOUT MIRRORING OR TERMINATE

// 4) IN 'AOM ACTIVITY' FLOW, 'These are EAI needs to be done' :
//    EAI GET NOTIFIED ABOUT CEASE, CHANGE OWNERSHIP, REMOVE OFFER, CHANGE MSISDN
//    EAI STORE THE NOTIFICATION IN DB

var moment = require ('moment');
var fs = require ('fs');

// FETCHING DATA SUBSCRIBER DAN DATA PELANGGAN DARI AOM
var DataSubscriberPaket = require ('./DataSubscriberPaketCPMirroring.json');
var DataPelanggan = require ('./DataPelanggan.json');

var DataYangAkanDiolah = [];
var sisa = [];

// CARI YANG "EXPIRED" DALAM ARTIAN MAU DILANJUT ATAU TERMINATE
DataSubscriberPaket.map (item => {
  var expired_date = moment (item.last_activation_timestamp).add (
    item.durasi,
    item.satuan_durasi
  );
  if (expired_date.isSameOrBefore (moment ())) {
    DataYangAkanDiolah.push (item);
  } else {
    sisa.push (item);
  }
});

// KIRIM SMS UTK CONFIRM TERMINATE / LANJOET
// GET RESPONSE DARI PELANGGAN
// FORWARD RESPONSE TO AOM
// GET RESPONSE FROM AOM (TERMINATE SUCCEED OR LANJOET)
DataYangAkanDiolah.map (item => {
  var namaPelanggan = DataPelanggan.find (itemPelanggan => {
    return itemPelanggan.msisdn === item.msisdn;
  }).nama;
  fs.appendFile (
    'LogCPMirroring.txt',
    namaPelanggan +' | ' + item.msisdn + ' dengan ' + item.nama_paket +' dikirim email konfirmasi lanjut/terminate\n'
    + namaPelanggan +' | ' + item.msisdn + ' dengan ' + item.nama_paket +' meminta untuk '+ require('./CPMirroringSMSResponse.json').permintaan +' product\n'
    + namaPelanggan +
    ' | ' +
    item.msisdn +
    ' dengan ' +
    item.nama_paket +
    ' berhasil ' + require('./CPMirroringSMSResponse.json').permintaan
    ,
    function (err) {
      if (err) throw err;
      console.log (
        namaPelanggan +
          ' | ' +
          item.msisdn +
          ' dengan ' +
          item.nama_paket +
          ' berhasil ' + require('./CPMirroringSMSResponse.json').permintaan
      );
    }
  );
});

// Passing to AOM
// Get Response From AOM (Mirroring or Terminate)
// UPDATE DB SUBSCRIBER
DataYangAkanDiolah.map ((item, index) => {
  var namaPelanggan = DataPelanggan.find (itemPelanggan => {
    return itemPelanggan.msisdn === item.msisdn;
  }).nama;
  if (require('./CPMirroringSMSResponse.json').permintaan === 'lanjut') {
    DataYangAkanDiolah[index].last_activation_timestamp = moment (
      item.last_activation_timestamp
    )
      .add (item.durasi, item.satuan_durasi)
      .format ('YYYY-MM-DDTHH:mm:ss');
  }
});
fs.writeFile (
  'DataSubscriberPaketCPMirroring.json',
  JSON.stringify (DataYangAkanDiolah.concat (sisa)),
  function (err) {
    if (err) throw err;
    console.log ('Saved!');
  }
);
