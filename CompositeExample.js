// MENG-IMPORT ABILITY UNTUK MANIPULASI FILE (FILE SYSTEM)
var fs = require ('fs');

// MENDEFINISIKAN PROSEDUR PROSES PERSIAPAN DENGAN 
var proses_persiapan = (x) => {
    return new Promise((resolve, reject) => {
        // MENDEFINISIKAN WAKTU PROSES INI HINGGA BERES DALAM 3 DETIK
        setTimeout(() => {
            resolve(x + ' | persiapan ...')
        }, 3000)
    })
}

// MENDEFINISIKAN PROSEDUR PROSES PENGAKTIFAN PAKET
var proses_utama = (x) => {
    return new Promise((resolve, reject) => {
         // MENDEFINISIKAN WAKTU PROSES INI HINGGA BERES DALAM 4 DETIK
        setTimeout(() => {
            resolve(x + ' | pengaktifan paket')
        }, 4000)
    })
}

// MENDEFINISIKAN PROSEDUR PROSES NOTIFIKASI SMS KE CUSTOMER
var proses_notifikasi = (x) => {
    return new Promise((resolve, reject) => {
         // MENDEFINISIKAN WAKTU PROSES INI HINGGA BERES DALAM 5 DETIK
        setTimeout(() => {
            resolve(x + ' | notifikasi sms ke customer\n')
        }, 5000)
    })
}

// MENDEFINISKAN PROSEDUR UNTUK PROSES YANG DIMULAI DARI TAHAP 1 (PROSES PERSIAPAN)
async function proses_mapping_1() {
    // MENDAPATKAN DATA DARI AOM
    var mock_data = require('./MOCK_DATA.json');
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST INDEX (ROW DATA YANG TERAKHIR DIPROSES SEBELUMNYA OLEH SCRIPT INI)
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST PROSES (PROSES YANG TERAKHIR DILAKUKAN SEBELUMNYA OLEH SCRIPT INI)
    fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, data){
        // MENGAMBIL DATA DARI AOM DENGAN INDEX = LAST INDEX
        var item = mock_data[JSON.parse(data).last_index];
        // MELAKUKAN PROSES PERSIAPAN
        await proses_persiapan(item.msisdn + ' | User Ke #' + item.id).then(async (resp1) => {
            await console.log(resp1)
            // MENGUPDATE PENANDA
            await fs.writeFile('penanda.json',
                JSON.stringify ({
                    last_index: Number(JSON.parse(data).last_index),
                    last_process : 1
                }),
                async function (err) {
                    if (err) throw err;
                    // MEMBACA PENANDA DARI FIlE penanda.json
                    await fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, dataX){
                        // LANJUT KE PROSES KE-2 
                        await proses_mapping_2()
                    })
                }
            );
        })
    }) 
}

// MENDEFINISIKAN PROSEDUR UNTUK PROSES YANG DIMULAI DARI TAHAP 2 (PROSES PENGAKTIFAN PAKET)
async function proses_mapping_2() {
    // MENDAPATKAN DATA DARI AOM
    var mock_data = require('./MOCK_DATA.json');
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST INDEX (ROW DATA YANG TERAKHIR DIPROSES SEBELUMNYA OLEH SCRIPT INI)
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST PROSES (PROSES YANG TERAKHIR DILAKUKAN SEBELUMNYA OLEH SCRIPT INI)
    fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, data){
        // MENGAMBIL DATA DARI AOM DENGAN INDEX = LAST INDEX
        var item = mock_data[JSON.parse(data).last_index];
        // MELAKUKAN PROSES PENGAKTIFAN PAKET
        await proses_utama(item.msisdn + ' | User Ke #' + item.id).then(async (resp2) => {
            await console.log(resp2)
            // MENGUPDATE PENANDA
            await fs.writeFile('penanda.json',
                JSON.stringify ({
                    last_index: Number(JSON.parse(data).last_index),
                    last_process : 2
                }),
                async function (err) {
                    if (err) throw err;
                    // MEMBACA PENANDA DARI FIlE penanda.json
                    await fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, dataX){
                        // LANJUT KE PROSES KE-3
                        await proses_mapping_3()
                    })
                }
            );
        })
    }) 
}

// MENDEFINISIKAN PROSEDUR UNTUK PROSES YANG DIMULAI DARI TAHAP 3 (PROSES NOTIF SMS KE CUSTOMER)
async function proses_mapping_3() {
    // MENDAPATKAN DATA DARI AOM
    var mock_data = require('./MOCK_DATA.json');
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST INDEX (ROW DATA YANG TERAKHIR DIPROSES SEBELUMNYA OLEH SCRIPT INI)
    // MEMBACA PENANDA DARI FILE penanda.json BERUPA LAST PROSES (PROSES YANG TERAKHIR DILAKUKAN SEBELUMNYA OLEH SCRIPT INI)
    fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, data){
        // MENGAMBIL DATA DARI AOM DENGAN INDEX = LAST INDEX
        var item = mock_data[JSON.parse(data).last_index];
        // MELAKUKAN PROSES PENGAKTIFAN PAKET
        await proses_notifikasi(item.msisdn + ' | User Ke #' + item.id).then(async (resp3) => {
            await console.log(resp3)
            // MENGUPDATE PENANDA
            await fs.writeFile('penanda.json',
                JSON.stringify ({
                    last_index: Number(JSON.parse(data).last_index+ 1),
                    last_process : 3
                }),
                async function (err) {
                    if (err) throw err;
                    // MEMBACA PENANDA DARI FIlE penanda.json
                    await fs.readFile("./penanda.json", {encoding: "utf8"}, async function(err, dataX){
                        // LANJUT KE PROSES AWAL (MUTAR SECARA REKURSIF)
                        await proses_mapping()
                    })
                    
                }
            );
        })
    }) 
}

// MENDEFINISIKAN PROSES REKURSIF DAN MENENTUKAN MULAI DARI MANA SCRIPT AKAN BERGERAK
async function proses_mapping() {
    var mock_data = require('./MOCK_DATA.json');
    await fs.readFile("./penanda.json", {encoding: "utf8"}, function(err, data) {
        // CEK JIKA DATA YANG DIPROSES MASIH ADA ATAU SUDAH HABIS (TERPROSES SEMUA)
        if (JSON.parse(data).last_index < mock_data.length) {
            // JIKA TERKAHIR SCRIPT INI DIJALANKAN DENGAN PROSES 1 UDAH SELESAI MAKA LANJUTKAN LANGSUNG KE PROSES 2
            if (JSON.parse(data).last_process === 1) {
                proses_mapping_2()
            } else {
                if (JSON.parse(data).last_process === 2) {
                    // JIKA TERKAHIR SCRIPT INI DIJALANKAN DENGAN PROSES 1 UDAH SELESAI MAKA LANJUTKAN LANGSUNG KE PROSES 3
                    proses_mapping_3()
                } else {
                    if (JSON.parse(data).last_process === 3) {
                        // JIKA TERKAHIR SCRIPT INI DIJALANKAN DENGAN PROSES 3 UDAH SELESAI MAKA LANJUTKAN LANGSUNG KE PROSES 1 (MUTAR)
                        proses_mapping_1()
                    } else {
                        // JIKA BELUM PERNAH DIJALANKAN SCRIPT INI MAKA MULAI DARI PROSES 1
                        proses_mapping_1()
                    }
                }
            }
        }
    }) 
}

// MENDEFINISIKAN MAIN PROSES DARI SCRIPT KOMPOSIT INI
async function main_process() {
    console.log('###### memulai script komposit #######')
    proses_mapping()
}

// EKSEKUSI PROSES YANG TELAH DIDEFINISIKAN 
main_process()
setTimeout(() => { process.exit(1); }, 60000);


