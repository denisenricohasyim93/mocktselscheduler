// BASED ON BR | AUTO RENEWAL USE CASE : 

// RUN EVERY TIME
// subscription to this product will be automatically renewed

// 1) IN 'ACTIVATION' FLOW, These are EAI needs to be done :
//    EAI GET NOTIFIED FROM AOM AND STORE IT TO DB WITH FIELDS :
//    # MSISDN, ACTION, SCH TYPE, NEWMSISDN, INSTANCE_ID, CATALOGUE ID, ATTR VALUE, 
//    # PERIOD, START, END DATE, LIST OF MESSAGE

// 2) IN 'SEND REMINDER (END-DATE X)' FLOW, These are EAI needs to be done :
//    EAI SEND REMINDER VIA SMS FW TO SMSGW TO SUBSCRIBER

// 3) IN 'DEACTIVATION REQUEST' FLOW, These are EAI needs to be done :
//    EAI GET NOTIFIED BY AOM ABOUT DEACTIVATION REQUEST AND STORE IT IN DB

// 4) IN 'RENEWAL EXECUTION' FLOW, These are EAI needs to be done :
//    EAI PERIODICALLY CHECK ITS DB IF 'SOMETHING' ABOUT TO BE RENEWED
//      EAI CHARGE TO INGW
//      EAI CREATE MEMO AND FWD IT TO AOM
//      EAI SEND RENEWAL SUCCESS VIA SMS
//    EAI PERIODICALLY CHECK ITS DB IF 'SOMETHING' ABOUT TO BE REMOVED
//      EAI REMOVE OFFER TO AOM
//      EAI GET NOTIFIED ABOUT REMOVED OFFER 



var moment = require ('moment');
var fs = require ('fs');
var credentials = require ('./Credentials.json');

// FETCHING DATA SUBSCRIBER DAN DATA PELANGGAN
var DataSubscriberPaket = require ('./DataSubscriberPaketAutoRenewal.json');
var DataPelanggan = require ('./DataPelanggan.json');

// SIAPKAN PENAMPUNG
var yangAkanDiolah = [];
var sisa = [];

// CARI YANG "EXPIRED" DALAM ARTIAN MAU DIRENEWAL
DataSubscriberPaket.map (item => {
  var expired_date = moment (item.last_activation_timestamp).add (
    item.durasi,
    'days'
  );
  // console.log(expired_date)
  if (expired_date.isSameOrBefore (moment ())) {
    yangAkanDiolah.push (item);
    // console.log('ada yang diolah bro')
  } else {
    sisa.push (item);
  }
});

// RENEWAL PACAKGE DENGAN UPDATE LAST ACTIVATION TIMESTAMP
yangAkanDiolah.map ((item, index) => {
  var namaPelanggan = DataPelanggan.find (itemPelanggan => {
    return itemPelanggan.msisdn === item.msisdn;
  }).nama;

  yangAkanDiolah[index].last_activation_timestamp = moment (
    item.last_activation_timestamp
  )
    .add (item.durasi, 'days')
    .format ('YYYY-MM-DDTHH:mm:ss');
  // console.log(yangAkanDiolah[index])

  fs.appendFile (
    'LogAutoRenewal.txt',
    namaPelanggan +
      ' | ' +
      item.msisdn +
      ' dengan ' +
      item.nama_paket +
      ' berhasil diautorenewal\nKirim SMS Notif Dengan Sender No ' +
      credentials.sms_number +
      '\n',
    function (err) {
      if (err) throw err;
      console.log (
        namaPelanggan +
          ' | ' +
          item.msisdn +
          ' dengan ' +
          item.nama_paket +
          ' berhasil diautorenewal'
      );
    }
  );
});

// console.log(yangAkanDiolah)
// console.log(yangAkanDiolah.concat(sisa))

// UPDATE DB SUBSCRIBER
fs.writeFile (
  'DataSubscriberPaketAutoRenewal.json',
  JSON.stringify (yangAkanDiolah.concat (sisa)),
  function (err) {
    if (err) throw err;
    console.log ('Saved!');
  }
);
